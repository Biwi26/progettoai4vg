using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidWallAvoidance : BoidComponent
{

	private Vector3 spawnerPos;

    private void Awake()
    {
		spawnerPos = GameObject.FindGameObjectWithTag("Spawner").transform.position;

	}
    public override Vector3 GetDirection(Collider[] neighbours, int size)
    {
		Vector3 wallAvoidance = Vector3.zero;
		Vector3 tmp;
		float wallsCounter = 0f;
		for (int i = 0; i < size; i++)
		{
			if (neighbours[i].gameObject.layer == 10) {
				tmp = transform.position - neighbours[i].ClosestPointOnBounds(transform.position);
				wallAvoidance += tmp.normalized / (tmp.magnitude + 0.00001f);
				wallsCounter ++;
            }
		}
		if (wallsCounter > 0f) { 
			wallAvoidance /= wallsCounter;
		}
		return (wallAvoidance.normalized ) * BoidShared.WallAvoidanceComponent;
	}
}
