using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidSurvival : BoidComponent
{
	float hunger = 0f;
	//float hungerMargin = 0.2f;
	//bool hungry = false;
	public bool goingForFood = false;
	Vector3 currentFoodPos = Vector3.zero;
	Vector3 finalDirection;

	bool mostHungryOne = true;


	private float randomCtr = 0f;
	private float randomCycle = 2f;

	override public Vector3 GetDirection(Collider[] neighbours, int size)
	{

		hunger = GetHunger();
		// set different random values for hunger margin from spawner
		//hungry = hunger > hungerMargin;

		currentFoodPos = GetComponentInChildren<VisionController>().GetNearestFood(transform.position);

		finalDirection = Vector3.zero;

		if (currentFoodPos != Vector3.zero) {                               // food available
			for (int i = 0; i < size; i++)
			{
				if (neighbours[i].gameObject.layer == gameObject.layer)
				{
					float neighbourHunger = neighbours[i].gameObject.GetComponent<HealthHandler>().GetMissingHPPerc();
					if (neighbourHunger > hunger)
					{
						mostHungryOne = false;
						neighbours[i].gameObject.GetComponent<BoidSurvival>().SetMostHungryOne();
						break;
					}
				}
			}
			if (mostHungryOne) {											// if I'm the most hungry one
				finalDirection += (currentFoodPos - transform.position);	// I'll go for food
			}
			else {															// otherwise I'll wander to find one
				randomCtr += Time.deltaTime;
				if (randomCtr >= randomCycle)
				{
					finalDirection += GenerateRandomDeviation();
					randomCtr -= randomCycle;
				}
			}

        }
        else																// no food, randomly wander
        {
			randomCtr += Time.deltaTime;
			if (randomCtr >= randomCycle) {
				finalDirection += GenerateRandomDeviation();
				randomCtr -= randomCycle;
            }
        }
		
		return finalDirection.normalized * (0.5f + hunger) * BoidShared.SurvivalComponent;
	}


	//public bool GetHungry()
 //   {
	//	return hungry;
 //   }

	public void ResetGoingForFood()
    {
		goingForFood = false;
    }

	public float GetHunger()
    {
		return gameObject.GetComponent<HealthHandler>().GetMissingHPPerc();
	}

	public bool GetFoodFound() {
		return gameObject.GetComponentInChildren<VisionController>().FoodAvailable();
    }

	public void SetMostHungryOne()
    {
		mostHungryOne = true;
    }

    private void OnTriggerEnter(Collider other)
    {
		if (other.gameObject.layer == 11 && other.transform.position == currentFoodPos 
										 && (other.transform.position - transform.position).magnitude < 1f) {

			other.gameObject.GetComponent<FoodHandler>().Eat(gameObject);
			
			Collider[] boidColliders = new Collider[200];
			int boidLayerMask = 1 << 8;
			int count = Physics.OverlapSphereNonAlloc(transform.position, BoidShared.BoidFOV, boidColliders, boidLayerMask);
			for (int i=0; i < count; i++) {
				boidColliders[i].gameObject.GetComponentInChildren<VisionController>().FoodGone(other.transform.position);
			}
			currentFoodPos = Vector3.zero;
		}
    }

	private Vector3 GenerateRandomDeviation()
	{
		float randomXDev = Random.Range(-1f, 1f);
		float randomYDev = Random.Range(-0.8f, 0.8f);
		float randomZDev = Random.Range(-1f, 1f);
		return new Vector3(randomXDev, randomYDev, randomZDev);

	}

}
