﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidCohesion : BoidComponent {

	override public Vector3 GetDirection (Collider [] neighbours, int size) {
		Vector3 cohesion = Vector3.zero;
		float counter = 0f;
		for (int i = 0; i < size; i += 1) {
			if (neighbours [i].gameObject.layer == gameObject.layer) {
				cohesion += neighbours [i].transform.position;
				counter += 1f;
			}
		}
		cohesion /= (float) counter;
		cohesion -= transform.position;
		return cohesion.normalized * BoidShared.CohesionComponent;
	}
}