﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidAlign : BoidComponent {

	override public Vector3 GetDirection (Collider [] neighbours, int size) {

		Vector3 alignment = Vector3.zero ;
		for (int i = 0; i < size; i +=1) {
			if (neighbours[i].gameObject.layer == gameObject.layer) {
				alignment += neighbours [i].gameObject.transform.forward;
			}
		}
		return alignment.normalized * BoidShared.AlignComponent;
	}

}
