﻿using UnityEngine;

public abstract class BoidComponent : MonoBehaviour {
	public abstract Vector3 GetDirection (Collider [] neighbors, int size);
}

public class BoidBlending : MonoBehaviour
{

	private Collider[] neighbours = new Collider[200];

	void FixedUpdate()
	{

		Vector3 globalDirection = Vector3.zero;

		// extract neighbouring walls and boids layers only
		int boidWallsLayerMask = 101 << 8;
		int count = Physics.OverlapSphereNonAlloc(transform.position, BoidShared.BoidFOV, neighbours, boidWallsLayerMask);
		foreach (BoidComponent bc in GetComponents<BoidComponent>())
		{
			globalDirection += bc.GetDirection(neighbours, count);
		}

		if (globalDirection != Vector3.zero)
		{
			transform.rotation = Quaternion.LookRotation((globalDirection.normalized + transform.forward) / 2f);
			//transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(globalDirection.normalized), BoidShared.BoidSpeed * Time.deltaTime);
		}

		transform.position += transform.forward * BoidShared.BoidSpeed * Time.deltaTime;
	}

}
