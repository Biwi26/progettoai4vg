using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionController : MonoBehaviour
{

    Vector3 nearestFoodPos;

    private void Start()
    {
        nearestFoodPos = Vector3.zero;
    }

    private void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.layer == 11) {
            SetNearestFood(other.transform.position);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        
    }

    private void OnTriggerExit(Collider other)
    {
        
    }


    public void FoodGone(Vector3 foodPos)
    {
        if (nearestFoodPos == foodPos)
            ResetNearestFood();
    }

    public bool FoodAvailable() {
        return nearestFoodPos != Vector3.zero;
    }

    public Vector3 GetNearestFood(Vector3 boidPosition)
    {
        return nearestFoodPos;
    }

    private void SetNearestFood(Vector3 newFoodPos)
    {
        
        if (nearestFoodPos == Vector3.zero) {
            nearestFoodPos = newFoodPos;
        }
        else {
            var curDist = (nearestFoodPos - transform.position).magnitude;
            var newDist = (newFoodPos - transform.position).magnitude;
            if (curDist > newDist) {
                nearestFoodPos = newFoodPos;
            }
        }
    }

    public void ResetNearestFood()
    {
        nearestFoodPos = Vector3.zero;
    }


}
