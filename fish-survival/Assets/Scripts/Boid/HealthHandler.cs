using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthHandler : MonoBehaviour
{
    // Start is called before the first frame update
    private float startHP = 100f;
    private float hp = 0f;

    [Header("HP Bar Image")]
    public Image healthBar = null;

    private void Start()
    {
        hp = startHP;
    }

    // Update is called once per frame
    private void Update()
    {
        hp -= Time.deltaTime;
        AdjustHealthBar();
        if (hp <= 0f)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }

    public float GetMissingHPPerc()
    {
        return (startHP - hp) / startHP;
    }

    public void RestoreHP(float amount)
    {
        hp += amount;
        if (hp > startHP)
            hp = startHP;
        AdjustHealthBar();
    }

    public void AdjustHealthBar()
    {
        if (healthBar != null) {
            healthBar.fillAmount = (float) hp / (float) startHP;

        }
    }

}
