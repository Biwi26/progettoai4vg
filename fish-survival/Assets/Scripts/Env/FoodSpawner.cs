using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSpawner : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject foodPrefab = null;
    
    [Range(0f, 100f)] public float spawnRadius = 10f;
    [Range(0f, 10f)] public float spawnCooldown = 1f;
    [Range(0, 10)] public int initialFood = 2;
    
    private float spawnCtr;
    private int foodSeq;

    void Start()
    {
        spawnCtr = 0f;
        foodSeq = 0;
        if (initialFood > 0) {
            if (foodPrefab != null) {
                for (int i = 0; i < initialFood; i++) {
                    SpawnFood();
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        spawnCtr += Time.deltaTime;
        if (spawnCtr >= spawnCooldown)
        {
            SpawnFood();
            spawnCtr -= spawnCooldown;
        }
    }
    
    private void SpawnFood()
    {
        GameObject go = Instantiate(foodPrefab, GenerateRandomPosition(), transform.rotation);
        go.name = foodPrefab.name + " " + foodSeq++;
    }
    
    private Vector3 GenerateRandomPosition()
    {
        float x = Random.Range(transform.position.x - spawnRadius, transform.position.x + spawnRadius);
        float y = Random.Range(transform.position.y - spawnRadius, transform.position.y + spawnRadius);
        float z = Random.Range(transform.position.z - spawnRadius, transform.position.z + spawnRadius);
        return new Vector3(x, y, z);
    }

}
