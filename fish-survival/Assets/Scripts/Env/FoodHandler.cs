using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodHandler : MonoBehaviour
{

    private float currentSize;
    private float startSize;
    private Vector3 baseScale;

    private float foodDuration = 0f;

    public Image foodBar;



    // Start is called before the first frame update
    void Start()
    {
        baseScale = transform.localScale;
        startSize = Mathf.Floor(Random.Range(2f, 5.999f));
        currentSize = startSize;
        transform.localScale *= startSize;
    }

    // Update is called once per frame
    void Update()
    {
        //FoodBobble();
        //AutoEatFood();
    }

    public void Eat(GameObject eater)
    {
        eater.GetComponent<HealthHandler>().RestoreHP(currentSize * 10);
        GameObject parent = transform.parent.gameObject;
        parent.SetActive(false);
        Destroy(parent);
    }

    void FoodBobble()
    {
        float variation = (Mathf.Cos(Time.realtimeSinceStartup * 2)) * 0.015f / 2f;
        transform.Translate(new Vector3(0f, variation, 0f));
    }


    void AutoEatFood()
    {
        foodDuration += 0.2f * Time.deltaTime;
        if (foodDuration >= 1f)
        {
            currentSize -= 1;
            foodDuration = 0f;
            AdjustFoodSize();
            AdjustFoodBar();
        }
    }

    void AdjustFoodBar()
    {
        foodBar.fillAmount = (float)currentSize / (float)startSize;
    }

    void AdjustFoodSize()
    {
        transform.localScale = baseScale * currentSize;
    }

    public float getFoodSize()
    {
        return currentSize;
    }

    //private void OnDestroy()
    //{
    //    Collider[] fishColliders = new Collider[200];
    //    int boidLayerMask = 1 << 9;
    //    int fishCount = Physics.OverlapSphereNonAlloc(transform.position, 20f, fishColliders, boidLayerMask);
    //    if (fishCount > 0) { 
    //        foreach (Collider col in fishColliders) {
    //            col.gameObject.GetComponent<VisionController>().FoodGone(transform.position);
    //        }
    //    }
    //}

}
